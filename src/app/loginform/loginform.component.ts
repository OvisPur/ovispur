import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';

@Component({
  selector: 'app-loginform',
  templateUrl: './loginform.component.html',
  styleUrls: ['./loginform.component.css']
})
export class LoginformComponent implements OnInit {

  rForm: FormGroup;
  post: any;
  password: string;
  name: string;
  titlealert = 'This field is required';

  constructor(private fb: FormBuilder) {

  this.rForm = fb.group({
    'name': [null, Validators.required],
    'password': [null, Validators.required],
    'validate': ''
  });
   }

  addPost(post) {
    this.password = post.password;
    this.name = post.name;
  }

  ngOnInit() {
    //this.rForm.get('validate').valueChanges.subscribe()
  }

}
